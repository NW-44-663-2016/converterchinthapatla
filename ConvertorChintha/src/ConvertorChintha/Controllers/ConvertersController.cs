using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using ConvertorChintha.Models;

namespace ConvertorChintha.Controllers
{
    public class ConvertersController : Controller
    {
        private ApplicationDbContext _context;

        public ConvertersController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Converters
        public IActionResult Index()
        {
            return View(_context.Converter.ToList());
        }

        // GET: Converters/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Converter converter = _context.Converter.Single(m => m.ConverterID == id);
            if (converter == null)
            {
                return HttpNotFound();
            }

            return View(converter);
        }

        // GET: Converters/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Converters/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Converter converter)
        {
            if (ModelState.IsValid)
            {
                _context.Converter.Add(converter);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(converter);
        }

        // GET: Converters/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Converter converter = _context.Converter.Single(m => m.ConverterID == id);
            if (converter == null)
            {
                return HttpNotFound();
            }
            return View(converter);
        }

        // POST: Converters/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Converter converter)
        {
            if (ModelState.IsValid)
            {
                _context.Update(converter);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(converter);
        }

        // GET: Converters/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Converter converter = _context.Converter.Single(m => m.ConverterID == id);
            if (converter == null)
            {
                return HttpNotFound();
            }

            return View(converter);
        }

        // POST: Converters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Converter converter = _context.Converter.Single(m => m.ConverterID == id);
            _context.Converter.Remove(converter);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
